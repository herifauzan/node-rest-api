var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    fullname: String,
    username: String,
    email: String,
    password: String,
    address: String,
    postalcode: Number,
    phonebook: String,
    role: String
});

module.exports = mongoose.model('User', userSchema);